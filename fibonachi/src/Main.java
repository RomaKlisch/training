
public class Main {

    public static void main(String[] args) {

        System.out.println(recurs(13));
    }

    public static int recurs(int x){

        if (x==0){
            return 0;
        }
        if (x==1){
            return 1;
        }
        else {

            return recurs(x-2)+recurs(x-1);
        }
        }

}
