package cat;

/**
 * Created by 123 on 15.02.2015.
 */
public class Cat {
   private int age;

    public Cat(int age, String poroda, String name) {
        this.age = age;
        this.poroda = poroda;
        this.name = name;
    }
    public Cat() {

    }

    private String poroda;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPoroda() {
        return poroda;
    }

    public void setPoroda(String poroda) {
        this.poroda = poroda;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public void say(){
        System.out.println("hru-hru");
    }
    @Override
    public String toString() {
        return (name+" "+poroda+" "+age);
    }
}
