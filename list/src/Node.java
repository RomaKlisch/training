import java.util.Random;

public class Node {
    int value;
    Node next;

    public Node(int value, Node node) {
        this.value = value;
        this.next = node;
    }

    public Node() {
    }

    public static Node generationIter(int num){
        Random random=new Random();
        Node rezult=null;
        for (int i = 0; i <num ; i++) {
            rezult=new Node(random.nextInt(100),rezult);
        }
        return rezult;
    }
    public static String toStringNodeRec(Node node){
        if (node==null){
            return "null";
        }
        else {
            return " "+node.value+" "+toStringNodeRec(node.next);
        }
    }

    public static int size(Node node){
        int size=1;
        Node ref=node.next;
        while (ref!=null){
         ref=ref.next;
            size++;
        }
        return size;
    }
    public static int sizeRec(Node node){
        int size=0;
     while (node.next!=null){
         sizeRec(node.next);
         size++;
     }
        return size;

    }
    public static void addIter(Node node, int value ){

        while (true) {
            if (node.next == null) {
                node.next = new Node(value, null);
                break;
            } else {
                {
                    node = node.next;
                }
            }
        }
    }

    public static void addIter(Node node,int value,int index){
         int size=0;
         int values;
        while (size<index) {
            if (size == index - 1) {
                node.next = new Node(value, node.next);
            }
            node = node.next;
            size++;
        }
    }

}
