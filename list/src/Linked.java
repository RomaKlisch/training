import java.util.Random;

public class Linked {

    private int data;
    Linked previous;
    Linked next;

    public Linked(int data, Linked previous, Linked next) {
        this.data = data;
        this.previous = previous;
        this.next = next;
    }
    public Linked() {

    }

    public static Linked generatorIter(int num){
        Random random=new Random();
        Linked rezultFirst=new Linked();
        rezultFirst.data=random.nextInt(100);

        Linked itemp, iiterrm;
        itemp=rezultFirst;

        for (int i = 0; i <num ; i++) {
            itemp.next=new Linked();
            iiterrm=itemp;
            itemp=itemp.next;
            itemp.previous=iiterrm;
            itemp.data=random.nextInt(100);
        }
        return rezultFirst;
    }

}
