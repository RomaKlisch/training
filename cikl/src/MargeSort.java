import java.util.Arrays;
import java.util.Random;

public class MargeSort {

    public static Random generators=new Random();


    public int[] createArray(int arraylight){
        int[] arrays=new int[arraylight];
        for (int i = 0; i <arraylight ; i++) {
            arrays[i]=generators.nextInt(100);
        }
        return arrays;
    }
    public  void printArryas(int[] array){
        for (int i = 0; i <array.length ; i++) {
            System.out.print(array[i]+"  ");
        }
    }

    public  int[] marge(int[]left, int[]right){
        int a=0, b=0;
        int[] margeArray=new int[left.length+right.length];
      while (a<left.length && b<right.length){
          if (left[a]<=right[b]){
              margeArray[a+b]=left[a];
              a++;
          }
          else if (left[a]>right[b]){
              margeArray[a+b]=right[b];
              b++;
          }

        }
        while (a<left.length){
            margeArray[a+b]=left[a];
            a++;
        }
        while (b<right.length){
            margeArray[a+b]=right[b];
            b++;
        }
        return margeArray;

    }

    public  int[] MSort( int[] arrays){
        int lenght=arrays.length;
        int middle=lenght/2;

        if (middle==0){
            return arrays;
        }

       int[] left = MSort(Arrays.copyOfRange(arrays,0,middle));
       int[] right = MSort(Arrays.copyOfRange(arrays,middle,lenght));

        return marge(left,right);
    }


    public static void main(String[] args) {

        MargeSort margeSort=new MargeSort();

        int[] array=margeSort.createArray(10);
        margeSort.printArryas(array);


        System.out.println("////////////////");
        margeSort.printArryas(margeSort.MSort(array));


    }


}
