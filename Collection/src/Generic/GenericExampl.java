package Generic;


public class GenericExampl {
    public static void main(String[] args) {

        GenericHolder<String> strData=new GenericHolder<>();
        strData.setData("Hello");

        GenericHolder<int []> arrInt=new GenericHolder<>();
        arrInt.setData(new int[]{2,6});

    }
}
