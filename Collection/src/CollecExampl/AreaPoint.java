package CollecExampl;


import java.util.Collection;
import java.util.Comparator;
import java.util.NavigableSet;
import java.util.TreeSet;

public class AreaPoint {

    private final NavigableSet<Point> xIndex=new TreeSet<>(new Comparator<Point>() {
        @Override
        public int compare(Point o1, Point o2) {
            return o1.getX()-o2.getX();
        }
    });
    private final NavigableSet<Point> yIndex=new TreeSet<>(new Comparator<Point>() {
        @Override
        public int compare(Point o1, Point o2) {
            return o1.getY()-o2.getY();
        }
    });
    void addPoint(Point point){
        xIndex.add(point);
        yIndex.add(point);
    }
    Collection<Point> selectPoint(int leftX, int rightX, int bottonY, int topY ){
        Point leftE=new Point(leftX,0);
        Point rightE=new Point(rightX,0);
        Point bottonE=new Point(0,bottonY);
        Point topE=new Point(0,topY);

        Collection<Point> xSelection=xIndex.subSet(leftE,true,rightE,true);
        Collection<Point> ySelection=yIndex.subSet(bottonE,true,topE,true);

        xSelection.retainAll(yIndex);
        return xSelection;
    }






}
