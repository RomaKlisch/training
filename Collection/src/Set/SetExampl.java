package Set;


import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class SetExampl {

    public static void main(String[] args) {
/*
        SortedSet<ComporableUser> set=new TreeSet<>();
        set.add(new ComporableUser(1,"b"));
        set.add(new ComporableUser(3,"a"));
        set.add(new ComporableUser(2,"c"));

        System.out.println(set);*/


        SortedSet<User> set=new TreeSet<>(new ComporatorUser());
        set.add(new User(1,"b"));
        set.add(new User(3,"a"));
        set.add(new User(2,"c"));

        System.out.println(set);

    }
}
