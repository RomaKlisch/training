package Set;



public class ComporableUser implements Comparable<ComporableUser> {
    private int age;
    private String name;

    public ComporableUser(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return  "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(ComporableUser o) {
        return this.age-o.age;
        /*return this.getName().compareTo(o.getName());*/

    }








































}
