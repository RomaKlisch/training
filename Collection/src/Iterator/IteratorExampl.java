package Iterator;


public class IteratorExampl {
    public static void main(String[] args) {
        Comp comp=new Comp();
        Emploee[]emploees={new Emploee("Mike"),new Emploee("Sara"),new Emploee("Serg")};

        comp.setEmploees(emploees);

        for(Emploee emp:comp){
            System.out.println(emp);
        }
    }
}
