package Iterator;


public class Emploee {
    String name;

    public Emploee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Emploee{" +
                "name='" + name + '\'' +
                '}';
    }
}
