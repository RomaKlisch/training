package Iterator;


import java.util.Iterator;
import java.util.function.Consumer;

public class Comp implements Iterable<Emploee> {

    private Emploee[]emploees=new Emploee[0];

    public void setEmploees(Emploee[] emploees) {
        this.emploees = emploees;
    }

    @Override
    public Iterator<Emploee> iterator() {
        return new ArrayIterator(emploees);
    }
}
