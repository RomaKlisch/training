

public class ApplicationExeption extends Exception{
    public ApplicationExeption(String message) {
        super(message);
    }

    public ApplicationExeption(String message, Throwable cause) {
        super(message, cause);
    }
}
