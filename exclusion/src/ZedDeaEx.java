
public class ZedDeaEx extends Exception {
    public ZedDeaEx(String message) {
        super(message);
    }

    public ZedDeaEx(String message, Throwable cause) {
        super(message, cause);
    }
}
