package TcharSet;


import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.SortedMap;

public class Main {
    public static void main(String[] args) throws IOException {

exampl2();



    }

public static void exampl1() throws IOException {
    byte[] rawData=new byte[256];
    for (int i = 0; i <256 ; i++) {
        rawData[i]=(byte)(i-128);
    }
    InputStream in=new ByteArrayInputStream(rawData);
    Reader reader=new InputStreamReader(in/*,"UTF-8"*/);

    char[] buff=new char[16];
    int count;
    while ((count=reader.read(buff))!=-1){
        System.out.println(new String(buff,0,count));
    }

}

public static void exampl2() throws IOException {
    InputStream in=null;

    try {
        in= new URL("https://vk.com/im?sel=202913958").openStream();
        Reader reader=new InputStreamReader(in,"UTF-8");

        char[] buff=new char[256];
        int count;
        while ((count=reader.read(buff))!=-1){
            System.out.println(new String(buff,0,count));
            System.out.println("\n");
        }

    }finally {
        if (in!=null){
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    }
}

















