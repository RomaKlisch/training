package DataInOut;


import java.io.*;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Main {
    public static void main(String[] args) throws IOException {

        writeToFile();
        readForm();

    }

    public static void writeToFile() throws IOException {

        byte age = 45;
        String name = "Rom";
        int[] salaryArr= {10,20,30,40};

        GZIPOutputStream outputStream=new GZIPOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream(
                        new File("C:/Users/123/IdeaProjects/NLO/Stream_IO/src/DataInOut/data.bin"))));
        DataOutput dataOutput=new DataOutputStream(outputStream);
        dataOutput.write(age);
        dataOutput.writeUTF(name);
        dataOutput.writeInt(salaryArr.length);
        for (int salaryElem:salaryArr) {
            dataOutput.writeInt(salaryElem);
        }
        outputStream.flush();
        outputStream.close();

        System.out.println();

    }
    public static void readForm() throws IOException {

        GZIPInputStream inputStream=new GZIPInputStream(
                new BufferedInputStream(
                        new FileInputStream(new File(
                                "C:/Users/123/IdeaProjects/NLO/Stream_IO/src/DataInOut/data.bin"
                        ))
                )
        );
        DataInputStream dataInput=new DataInputStream(inputStream);
        byte age=dataInput.readByte();
        String name=dataInput.readUTF();
        int[] salaryArr=new int[dataInput.readInt()];
        for (int i = 0; i <salaryArr.length ; i++) {
            salaryArr[i]=dataInput.readInt();
        }
        inputStream.close();
        System.out.println(age);
        System.out.println(name);
        System.out.println(Arrays.toString(salaryArr));
    }


















}
