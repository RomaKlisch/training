package AdapterDecorator;

import java.io.*;

public class Main {
    public static void main(String[] args) {

        String fileFrom="C:/Users/123/IdeaProjects/NLO/Stream_IO/src/AdapterDecorator/img1.jpg";
        String fileTo="C:/Users/123/IdeaProjects/NLO/Stream_IO/src/AdapterDecorator/img2.jpg";


        InputStream in=null;
        OutputStream out=null;

        try {
            in= new BufferedInputStream(new FileInputStream(fileFrom));
            out=new BufferedOutputStream(new FileOutputStream(fileTo));
            copy1(in,out);
        }catch (IOException e){

        }finally {
            closeStreamIn(in);
            closeStreamOut(out);
        }
    }

    public static synchronized void copy1(InputStream in, OutputStream out) throws IOException {
        int onebyte;
        while ((onebyte=in.read())!=-1){
            out.write(onebyte);
        }
    }
    public static void copy2(InputStream in, OutputStream out) throws IOException{
        byte[] buff=new byte[256];
        int count;
        while ((count=in.read(buff))!=-1){
            out.write(buff,0,count);
        }
    }

    public static void closeStreamIn(InputStream in){
        try {
            in.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
    }
    public static void closeStreamOut(OutputStream out){
        try {
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
