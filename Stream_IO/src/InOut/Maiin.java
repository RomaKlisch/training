package InOut;

import java.io.*;
import java.util.Arrays;

public class Maiin {

    public static void main(String[] args) throws IOException {

        String filenameFrom = "C:/Users/123/IdeaProjects/NLO/Stream_IO/src/InOut/img1.jpg";
        String filenameTo = "C:/Users/123/IdeaProjects/NLO/Stream_IO/src/InOut/img2.jpg";

        InputStream in=null;
        OutputStream out=null;

        try {
            in= new FileInputStream(filenameFrom);
            out=new FileOutputStream(filenameTo);
            copy(in,out);
        } catch (IOException e) {
            throw new IOException("Exeption when copy",e);
        }finally {
            closeInStream(in);
            closeOutStream(out);

        }







        /*InputStream file1 = null;
        try {
            file1 = new FileInputStream(filename);
            //readFullyByte(file1);
            readByteToArray(file1);

        } catch (IOException e) {
            throw new IOException("dddddddddddddd" + e);
        } finally {
            closeInStream(file1);
        }
        System.out.println("\n\n\n");

      *//*  InputStream inUrl = new URL("https://www.google.com.ua/").openStream();
        readFullyByte(inUrl);
        System.out.println("\n\n\n");*//*

        InputStream inArray = new ByteArrayInputStream(new byte[]{65, 66, 67});
        try {
            readFullyByte(inArray);

        } catch (IOException e) {
            throw new IOException("dddddddddddddd" + e);
        } finally {
            closeInStream(inArray);
        }
        System.out.println("\n\n\n");*/
    }
    public static void copy(InputStream in,OutputStream out) throws IOException {
byte [] buff=new byte[64*1024];
        int count;
  while (true){
            if ((count=in.read(buff))!=-1){
                out.write(buff,0,count);
            }
            else {
                break;
            }
        }
    }

    public static void readFullyByte(InputStream inputStream) throws IOException {
        while (true){
            int oneByte=inputStream.read();
            if (oneByte!=-1){
                System.out.print((char) oneByte);
            }
            else {
                System.out.println("  end");
                break;
            }

        }
    }
    public static void closeInStream(InputStream inputStream) {
        if (inputStream!=null){
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    public static void closeOutStream(OutputStream outputStream) throws IOException {
        if (outputStream!=null){
            outputStream.flush();
            outputStream.close();
        }
    }


    public static void readByteToArray(InputStream inputStream) throws IOException {
        byte[] aaa=new byte[5];

        while (true){
            int count=inputStream.read(aaa);
            if (count!=-1){
                System.out.println(Arrays.toString(aaa)+new String(aaa,0,count,"UTF8"));
            }
            else {
                break;
            }
        }

    }
    public static byte[] readFullToByte(InputStream inputStream) throws IOException {
        int oneByte;
        ByteArrayOutputStream outputStream=new ByteArrayOutputStream();
        while ((oneByte=inputStream.read())!=-1){
            outputStream.write(oneByte);
        }
        outputStream.flush();
        outputStream.close();
        return outputStream.toByteArray();
    }







}
