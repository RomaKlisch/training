package Synhron;


public class BufferedEx {

    private int waitedProducerc = 0;
    private int waitedConsumers = 0;
    private Integer elem=1;

    public synchronized void put(Integer newElem) throws InterruptedException {
        while (elem!=null){
            waitedProducerc++;
            this.wait();
            waitedProducerc--;
        }
        this.elem=newElem;
        if (waitedConsumers>0){
            this.notify();
        }
    }
    public synchronized Integer get() throws InterruptedException {
        while (elem==null){
            waitedConsumers++;
            this.wait();
            waitedConsumers--;
        }
        int rez=this.elem;
        this.elem=null;
        if (waitedProducerc>0){
            this.notify();
        }
        return rez;
    }

    public static void main(String[] args) throws InterruptedException {
        new BufferedEx().put(new BufferedEx().get());
    }
}
