
public class PrintRunnable  implements  Runnable {

    private String msg;

    private long sec;

    public PrintRunnable(String msg, long sec) {
        this.msg = msg;
        this.sec = sec;
    }

    @Override
    public void run()  {
        for (int i = 0; i <5 ; i++) {
            try {
                Thread.sleep(sec);
            } catch (InterruptedException e) {
               throw new RuntimeException(e);
            }
            System.out.println(msg);
        }

    }
}
