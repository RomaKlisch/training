

public class TheadExampl3 {
    public static void main(String[] args) throws InterruptedException {

        for (int i = 0; i <100 ; i++) {
            String spaces=spaces(i);
            Runnable printer=new PrintRunnable(spaces+i, 200);
            Thread thread=new Thread(printer);
            thread.start();
            Thread.sleep(100);
        }

    }

    public static String spaces(int count){
        String rez="";
        for (int i = 0; i <count ; i++) {

            rez+=" ";
        }
        return rez;
    }
}
