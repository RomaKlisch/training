

public class TheadExampl2 {
    public static void main(String[] args) throws InterruptedException {


        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    for (int i = 0; i < 5; i++) {
                        print();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();

    }

    public static void print() throws InterruptedException {
        Runnable print=new PrintRunnable("A  ",100);
        Thread thread=new Thread(print);
        thread.start();

        Runnable print2=new PrintRunnable("  B",99);
        Thread thread2=new Thread(print2);
        thread2.start();



        thread.join();


        System.out.println("---");

        thread2.join();
        Runnable print3=new PrintRunnable(" -C- ",200);
        Thread thread3=new Thread(print3);
        thread3.start();

        thread3.join();
        System.out.println("goodby");
    }
}
